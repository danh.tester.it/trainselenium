import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import time


class Login(unittest.TestCase):

    def __init__(self):
        self.driver = webdriver.Chrome(r"C:\Users\Diem Tran\Downloads\chromedriver_win32\chromedriver.exe")
        self.sWebURL = "https://www.saucedemo.com/index.html"
        self.sUser = "standard_user"
        self.sPassWord = "secret_sauce"
        self.sIDUser = "user-name"
        self.sIDPw = "password"
        self.sIDLoginBtn = "login-button"

    def __compareText(self, sActualtext, sExpectedText):

        try:
            print("Actual: {0}".format(sActualtext))
            print("Expected: {0}".format(sExpectedText))
            return sActualtext == sExpectedText
        except ValueError:
            return None

    def __login(self, sUser, sPW):

        self.driver.get(self.sWebURL)
        self.assertIn("Swag Labs", self.driver.title)
        self.driver.find_element_by_name(self.sIDUser).clear()
        self.driver.find_element_by_name(self.sIDUser).send_keys(sUser)
        self.driver.find_element_by_name(self.sIDPw).clear()
        self.driver.find_element_by_name(self.sIDPw).send_keys(sPW)
        self.driver.find_element_by_id(self.sIDLoginBtn).click()
        WebDriverWait(self.driver, 2)

    def validateLoginSuccessful(self, sUser, sPW):
        self.__login(sUser, sPW)
        if sUser == self.sUser and sPW == self.sPassWord:
            assert self.driver.find_element_by_class_name("main-body")
            print("Login Successfully")
        else:
            print("Login Un-successfully due to other reason")

        return

    def logout(self):
        try:
            self.driver.find_element_by_class_name('bm-burger-button').click()
            time.sleep(2)
            self.driver.find_element_by_xpath('/html/body/div/div[1]/div/div[2]/div[1]/nav/a[3]').click()
            time.sleep(2)
            assert self.driver.find_element_by_id(self.sIDLoginBtn)
            print("Logout Successfully")
            return True
        except "Logout unsuccessful":
            return False

    def validateLoginUnsuccessful(self, sUser, sPW):
        self.__login(sUser, sPW)
        sActualError = self.driver.find_element_by_xpath('//h3[@data-test=\'error\']').text
        if not sUser:
            assert self.__compareText(sActualError, "Epic sadface: Username is required")
        elif not sPW:
            assert self.__compareText(sActualError, "Epic sadface: Password is required")
        elif sUser != self.sUser or sPW != self.sPassWord:
            assert self.__compareText(sActualError, "Epic sadface: Username and password do not match any user in this service")
        return

    def addItemsByXPath(self, lItems):
        for sItem in lItems:
            self.driver.find_element_by_xpath(sItem).click()

        sNumItems = self.driver.find_element_by_xpath('/html/body/div/div[2]/div[1]/div[2]/a/span').text
        if self.__compareText(sNumItems, str(len(lItems))):
            print("Add {0} to cart successfully". format(len(lItems)))
            return True
        else:
            print("There are some items not added to cart.")
            return False

    def deleteItemsByXPath(self, lItems):
        for sItem in lItems:
            sText = self.driver.find_element_by_xpath(sItem).text
            if self.__compareText(sText, "REMOVE"):
                self.driver.find_element_by_xpath(sItem).click()
            else:
                print("Item has not added to cart")

    def checkoutItems(self):
        time.sleep(3)

        self.driver.find_element_by_class_name('shopping_cart_container').click()
        self.driver.find_element_by_class_name('btn_action').click()
        try:
            self.driver.find_element_by_xpath('/html/body/div/div[2]/div[1]/div[2]/a/span')
            self.driver.find_element_by_id("first-name").clear()
            self.driver.find_element_by_id("first-name").send_keys('Van')
            self.driver.find_element_by_id('last-name').clear()
            self.driver.find_element_by_id('last-name').send_keys('Anh')
            self.driver.find_element_by_id('postal-code').clear()
            self.driver.find_element_by_id('postal-code').send_keys('1234')
            self.driver.find_element_by_class_name('btn_primary').click()
            time.sleep(2)
            self.driver.find_element_by_class_name('btn_action').click()
            time.sleep(2)
            sComplete = self.driver.find_element_by_class_name('complete-header').text
            if self.__compareText(sComplete, "THANK YOU FOR YOUR ORDER"):
                print('The order is successful')
            else:
                print('The order is not successful')

        except:
            print('There is no item in cart')

        return

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    oLogin = Login()
    # Exe 1.1
    oLogin.validateLoginSuccessful(sUser="standard_user", sPW="secret_sauce")
    time.sleep(2)
    oLogin.logout()
    oLogin.validateLoginUnsuccessful(sUser="", sPW="secret_sauce")
    time.sleep(2)
    oLogin.validateLoginUnsuccessful(sUser="standard_user", sPW="")
    time.sleep(2)
    oLogin.validateLoginUnsuccessful(sUser="standard_user", sPW="Incorrect password")
    time.sleep(2)
    oLogin.validateLoginUnsuccessful(sUser="Incorrect user", sPW="Incorrect password")
    time.sleep(2)
    oLogin.validateLoginSuccessful(sUser="standard_user", sPW="secret_sauce")
    time.sleep(2)
    # Exe 1.2
    oLogin.addItemsByXPath(lItems=['/html/body/div/div[2]/div[2]/div/div[2]/div/div[1]/div[3]/button', '/html/body/div/div[2]/div[2]/div/div[2]/div/div[2]/div[3]/button', '/html/body/div/div[2]/div[2]/div/div[2]/div/div[3]/div[3]/button'])
    time.sleep(2)
    oLogin.deleteItemsByXPath(lItems=["/html/body/div/div[2]/div[2]/div/div[2]/div/div[1]/div[3]/button"])
    time.sleep(2)
    # Exe 1.3
    oLogin.checkoutItems()
    time.sleep(2)
    oLogin.tearDown()