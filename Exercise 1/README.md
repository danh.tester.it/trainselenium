**URL test**: https://www.saucedemo.com/index.html

**Username**: standard_user

**Password**: secret_sauce

**Use selenium and python to test the following:**


1. _Test login page_

- Login successfully
- Login with usename is null and verify pop-up error
- Login with password is null and verify pop-up error
- Login with password is incorrect and verify pop-up error

2. _Add/Remove item to cart_

- Select 3 item and add to cart and verify have 3 item on your cart
- Select 1 item and remove on your cart and verify 2 item on your cart

3. Checkout item to cart

- Open web browser and login
- Click button ‘Your cart’
- Click button ‘CHECKOUT’
- Put first name, last name, zip
- Click button ‘CONTINUES’
- Click button ‘FINISH’
- Verify order should be successful
