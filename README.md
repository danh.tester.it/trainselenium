**What is Selenium?**

- Selenium refers to a suite of tools that are widely used in the testing community when it comes to cross-browser testing. Selenium cannot automate desktop applications; it can only be used in browsers. It is considered to be one of the most preferred testing tool-suite for automating web applications as it provides support for popular web browsers which makes it very powerful.

**Browsers supported by Selenium:**

- Google Chrome 12+
- Internet Explorer 7,8,9,10
- Safari 5.1+
- Opera 11.5
- Firefox 3+
- Along with this, Selenium is also capable of working on multiple Operating Systems.

**Multiple Operating Systems supported by Selenium:**

- Windows
- Mac
- Linux/Unix
- Another reason why Selenium is gaining popularity is that it provides compatibility with different programming languages which makes it very flexible for the testers to design test cases.

**Programming Languages supported by Selenium**

- C#
- Java
- JavaScript
- Ruby
- Python
- PHP

**What is Selenium WebDriver? Why is it used?**

- Selenium WebDriver is a web framework that permits you to execute cross-browser tests. This tool is used for automating web-based application testing to verify that it performs expectedly.
- Selenium WebDriver allows you to choose a programming language of your choice to create test scripts. As discussed earlier, it is an advancement over Selenium RC to overcome a few limitations. Selenium WebDriver is not capable of handling window components, but this drawback can be overcome by using tools like Sikuli, Auto IT, etc.

**Selenium WebDriver Framework Architecture**
![alt text](https://3fxtqy18kygf3on3bu39kh93-wpengine.netdna-ssl.com/wp-content/uploads/2019/07/Selenium-WebDriver-for-Automation-Testing.jpg)

**Use-Case Scenario**

Understanding cross-browser testing automation using Selenium WebDriver with a complete scenario

Assuming in real time, you start writing the code in your User Interface (consider Eclipse IDE)
with the help of any of the client libraries supported by Selenium (say Python). You also need to have selenium web driver for Chrome if you prefer to use Chrome.

```
WebDriver driver = new ChromeDriver ();
driver. get (https://www.browserstack.com)
```

As soon as you complete writing your code, execute the program by clicking on Run. The above code will result in the launching of the Chrome browser which will navigate to the BrowserStack website.

Now let us understand what goes behind the scene when you click on Run until the launching of the Chrome Browser.
